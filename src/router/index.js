import Vue from 'vue'
import Router from 'vue-router'
import AdminIndex from '@/views/index'
import TopicView from '@/views/topicList'
import SoftJian from '@/views/softjian'
import Tlevel from '@/views/tlevel'
import Replys from '@/views/replys'
import slevel from '@/views/slevel'
import LoginView from '@/views/login'
import useroperationView from '@/views/useroperation'

Vue.use(Router)

Vue.use(Router)
    let routes= [{
        path: '/',
        name: 'AdminIndex',
        component: AdminIndex,
        children:[
            {
                path: '/useroperation',
                component: useroperationView
            },
			{
                path: '/topic',
                component: TopicView
            },
            {
                path: '/softjian',
                component: SoftJian
            },
            {
                path:"/slevel",
                component: slevel

            },
            {
                path: '/tlevel',
                component: Tlevel
            },
            {

                path: '/replys',
                component: Replys
            }
        ]
    },
    {
      path: '/login',
      component: LoginView
    }
    
]
const router = new Router({
    mode:'history',//hash模式
    routes
});

router.beforeEach(
(to,from,next)=>{
if(to.path === '/login'){
   next();
}else{
  let token  = localStorage.getItem('token');
// alert(token)
  if(token === null || token ===''){
    next('/login');
  }else{
    next();
  }
}
});

export default router 